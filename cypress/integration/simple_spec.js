describe('Los estudiantes login', function() {
    it('Visits los estudiantes and fails at login', function() {
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        cy.contains('Ingresar').click()
        cy.get('.cajaLogIn').find('input[name="correo"]').click().type("wrongemail@example.com")
        cy.get('.cajaLogIn').find('input[name="password"]').click().type("1234")
        cy.get('.cajaLogIn').contains('Ingresar').click()
        cy.contains('El correo y la contraseña que ingresaste no figuran en la base de datos. Intenta de nuevo por favor.')
    })

    it('Visists los estudiantes and succed at login', function(){
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        cy.contains('Ingresar').click()
        cy.get('.cajaLogIn').find('input[name="correo"]').click().type("wrongemail2@example.com")
        cy.get('.cajaLogIn').find('input[name="password"]').click().type("12345678")
        cy.get('.cajaLogIn').contains('Ingresar').click()
    })

    it('Visits los estudiantes, try to create an existing account', function(){
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        cy.contains('Ingresar').click()
        cy.get('.cajaSignUp').find('input[name="nombre"]').click().type("wronge")
        cy.get('.cajaSignUp').find('input[name="apellido"]').click().type("email2")
        cy.get('.cajaSignUp').find('input[name="correo"]').click().type("wrongemail2@example.com")
        cy.get('.cajaSignUp').find('select[name="idUniversidad"]').select('Universidad de los Andes')
        cy.get('.cajaSignUp').contains('Estudio una maestria').find('input[type="checkbox"]').check()
        cy.get('.cajaSignUp').find('select[name="idPrograma"]').select('Maestría en Ingeniería de Software')
        cy.get('.cajaSignUp').find('input[name="password"]').click().type("12345678")
        cy.get('.cajaSignUp').contains('Acepto los términos y condiciones y la política de privacidad').find('input[type="checkbox"]').check()
        cy.get('.cajaSignUp').contains('Registrarse').click()
        cy.get('.sweet-alert').contains('Ok').click()
    })
})

describe('Search a professor',function(){
    it('Search for Mario', function(){
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        cy.get('.Select-input').click({force: true }).find('input').type('Mario Linares',{ force: true});
    })
})

describe('Go to professor page', function(){
    it('select first professor on list', function(){
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        cy.get('.profesores').find('.profesor').first().find('img').click()
        cy.get('.descripcionProfesor')
    })
})

describe('Use class filters', function(){
    it('Select a course and expect others to not exist', function(){
        cy.visit('https://losestudiantes.co/universidad-de-los-andes/ingenieria-de-sistemas/profesores/mario-linares-vasquez')
        cy.get('.statsProfesorDropdown').find('.materias').find('input[type="checkbox"]').first().check()
        cy.get('.columnMiddle').find('.post').should('to.not.contain','Estructuras de Datos')
    })
    it('unselect a course and expect others to not exist', function(){
        cy.get('.statsProfesorDropdown').find('.materias').find('input[type="checkbox"]').first().uncheck()
    })
})