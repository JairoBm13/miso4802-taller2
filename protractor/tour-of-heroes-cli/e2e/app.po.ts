import {browser, by, element, ElementFinder} from 'protractor';

export class TourOfHeroesPage {
  navigateTo() {
    return browser.get('/');
  }

  getTop4Heroes() {
    return element.all(by.css('.module.hero')).all(by.tagName('h4')).getText();
  }

  navigateToHeroes() {
    element(by.linkText('Heroes')).click();
  }

  getAllHeroes() {
    return element(by.tagName('my-heroes')).all(by.tagName('li'));
  }

  enterNewHeroInInput(newHero: string) {
    element(by.tagName('input')).sendKeys(newHero);
    element(by.buttonText('Add')).click();
  }

  serchForHeroByName(name : string){
    element(by.css('#search-box')).sendKeys(name);
  }

  clickSearchedHero(){
    element(by.css(".search-result")).click();
  }

  lookForResultsHero(){
    return element.all(by.css('.search-result'));;
  }

  deleteHero(toDelete :number){
    element(by.css('.heroes')).all(by.tagName('li')).get(toDelete).element(by.css('.delete')).click();
  }

  selectHero(toSelect : number){
    element(by.css('.heroes')).all(by.tagName('li')).get(toSelect).element(by.css('.badge')).click();
  }

  selectTopHero(top : number){
    element.all(by.css('.module.hero')).get(top).click();
  }
}
