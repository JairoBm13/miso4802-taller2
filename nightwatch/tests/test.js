module.exports = { // adapted from: https://git.io/vodU0
    'Los estudiantes login falied': function(browser) {
      browser
        .url('https://losestudiantes.co/')
        .click('.botonCerrar')
        .waitForElementVisible('.botonIngresar', 4000)
        .click('.botonIngresar')
        .setValue('.cajaLogIn input[name="correo"]', 'wrongemail@example.com')
        .setValue('.cajaLogIn input[name="password"]', '1234')
        .click('.cajaLogIn .logInButton')
        .waitForElementVisible('.aviso.alert.alert-danger', 4000)
        .assert.containsText('.aviso.alert.alert-danger', 'El correo y la contraseña que ingresaste no figuran')
        .end();
    },
    'ir a la página de un profesor': function(browser){
        browser.url('https://losestudiantes.co')
        .click('.botonCerrar')
        .waitForElementVisible('.profesores', 4000)
        .click('.profesores .profesor img')
        .waitForElementVisible('.descripcionProfesor', 4000)
        .expect.element('.descripcionProfesor').to.be.visible;
    },
    'usar filtros de cursos':function(browser){
      browser.url('https://losestudiantes.co')
        .click('.botonCerrar')
        .waitForElementVisible('.profesores', 4000)
        .click('.profesores .profesor img')
        .waitForElementVisible('.descripcionProfesor', 4000)
        .click('.materias input[type="checkbox"]');
    }
  };


  /**
   * 

describe('Use class filters', function(){
    it('Select a course and expect others to not exist', function(){
        cy.visit('https://losestudiantes.co/universidad-de-los-andes/ingenieria-de-sistemas/profesores/mario-linares-vasquez')
        cy.get('.statsProfesorDropdown').find('.materias').find('input[type="checkbox"]').first().check()
        cy.get('.columnMiddle').find('.post').should('to.not.contain','Estructuras de Datos')
    })
    it('unselect a course and expect others to not exist', function(){
        cy.get('.statsProfesorDropdown').find('.materias').find('input[type="checkbox"]').first().uncheck()
    })
})
   */